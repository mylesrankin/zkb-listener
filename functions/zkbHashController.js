const model = require('../_data-model/index');
const zkbModel = model.zkbHashes;
const killmailHashQueue = model.killmailHashQueue;
const moment = require('moment');

const add = (zkbDateString, zkbHashes) => {
  return new Promise((resolve, reject) => {
    zkbModel
      .findOneAndUpdate(
        { zkbDateString },
        {
          $set: {
            zkbDateString: zkbDateString,
            zkbDate: moment(zkbDateString).format('YYYY-MM-DD'),
            killmails: zkbHashes,
          },
        },
        { upsert: true, new: true }
      )
      .then((doc) => resolve(doc))
      .catch((err) => reject(err));
  });
};

const getNextZkbDateString = () => {
  return new Promise((resolve, reject) => {
    zkbModel
      .findOne()
      .sort({ zkbDate: -1 })
      .then((res) => resolve(res))
      .catch((err) => reject(err));
  });
};

const getNextHashSetForQueue = () => {
  return new Promise((resolve, reject) => {
    zkbModel
      .findOne({ migrated: false || null })
      .sort({ zkbDate: 1 })
      .then((res) => resolve(res))
      .catch((err) => reject(err));
  });
};

const setMigrated = (_id, status = true) => {
  return new Promise((resolve, reject) => {
    zkbModel
      .findOneAndUpdate(
        { _id },
        { $set: { migrated: status } },
        { new: true, upsert: true }
      )
      .then((res) => resolve(res))
      .catch((err) => reject(err));
  });
};

const killmailQueueInsert = (killmailsToQueue) => {
  return new Promise((resolve, reject) => {
    console.log(`Queued ${killmailsToQueue.length} killmails`);
    killmailHashQueue
      .insertMany(killmailsToQueue, { ordered: false })
      .then((doc) => resolve(doc))
      .catch((err) => reject(err));
  });
};

module.exports = {
  add,
  getNextZkbDateString,
  getNextHashSetForQueue,
  setMigrated,
  killmailQueueInsert,
};
