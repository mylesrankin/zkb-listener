var mongoose = require('mongoose');
require('dotenv').config();

console.log(process.env.DB_CONNECTION_STRING);

process.env.DB_CONNECTION_STRING &&
  createConnection(process.env.DB_CONNECTION_STRING);
mongoose.Promise = global.Promise;

function dbDisconnect() {
  mongoose.connection.close().then(
    (success) => {
      console.log('Connection closed');
    },
    (err) => {
      console.log('Failed to close connection');
      console.log('Connection Close Error: ', err);
    }
  );
}

function createConnection(connectionString) {
  try {
    mongoose.Promise = global.Promise;
    mongoose.connect(connectionString, {
      useNewUrlParser: true,
      useFindAndModify: false,
    });

    var db = mongoose.connection;
    db.on('error', function() {
      console.error.bind(console, 'connection error:');
      mongoose.disconnect();
    });
    db.on('disconnected', function() {
      console.log('db disconnected..no retry');
    });

    db.once('open', function() {
      console.log('db connected: ' + process.env.DB_CONNECTION_STRING);
    });
    return db;
  } catch (err) {
    console.error(err);
  }
}

exports.createConnection = createConnection;
exports.dbDisconnect = dbDisconnect;
exports.zkbHashes = require('./schemas/zkbHashes');
exports.killmailHashQueue = require('./schemas/killmailHashQueue');
