var mongoose = require('mongoose');
var moment = require('moment');

var Schema = mongoose.Schema;

var zkbHashes = new Schema({
  zkbDateString: String,
  zkbDate: Date,
  killmails: Object,
  dateScraped: { type: Date, default: moment() },
  migrated: { type: Boolean, default: false },
});

module.exports = mongoose.model('zkbHashes', zkbHashes);
