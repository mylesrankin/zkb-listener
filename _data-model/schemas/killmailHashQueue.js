var mongoose = require('mongoose');
var moment = require('moment');

var Schema = mongoose.Schema;

var killmailHashQueue = new Schema({
  _id: String,
  killmail_id: String,
  killmail_hash: String,
  date: String,
  scrapedFromESI: { type: Boolean, default: false },
});

module.exports = mongoose.model('killmailHashQueue', killmailHashQueue);
