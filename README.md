# zkb-listener

This is a simple microservice that takes a websocket connection from zKillboard (https://github.com/zKillboard/zKillboard/wiki/Websocket), then both stores incoming killmails (in MongoDB) and publishes them to an AWS SNS Queue. 