require('dotenv').config();
const zkb = require('./functions/zkbHashController');
const moment = require('moment');
const fetch = require('node-fetch');

// Rate limiter
const RPS = 25;

let run = async () => {
  try {
    let nextDate = await zkb.getNextZkbDateString();
    if (nextDate === null) nextDate = moment();
    nextDate = await getNextDate(nextDate.zkbDate);
    do {
      let res = await scrapeDate(nextDate);
      zkb.add(nextDate, res);
      nextDate = await getNextDate(nextDate);
      await sleep((60 / (60 * RPS)) * 1000);
    } while (nextDate !== '20071203');
  } catch (err) {
    console.error(err);
  }
};

const scrapeDate = (date) => {
  return new Promise((resolve, reject) => {
    console.log(`scraping: ${date}`);
    fetch(`https://zkillboard.com/api/history/${date}.json`)
      .then((res) => res.json())
      .then((res) => resolve(res))
      .catch((err) => reject(err));
  });
};

const getNextDate = (date) => {
  return new Promise((resolve, reject) => {
    resolve(
      moment(date)
        .subtract(1, 'day')
        .format('YYYYMMDD')
    );
  });
};

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

run();
