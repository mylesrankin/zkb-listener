require('dotenv').config();
const WebSocket = require('ws');
const moment = require('moment');
const { publishToSNS } = require('./functions/snsHelper');

const ws = new WebSocket('wss://zkillboard.com:2096');

ws.on('open', function open() {
  console.log(process.env.AWS_PROFILE);
  console.info(`${moment().format()} :: Opened zKillboard Websocket`);
  ws.send(
    JSON.stringify({
      action: 'sub',
      channel: 'killstream',
    })
  );
});

ws.on('message', function incoming(data) {
  console.log(`Received KM: ${JSON.parse(data).zkb.url}`);
  publishToSNS({ killmail: data }, 'killmail-stream', {
    type: {
      DataType: 'String',
      StringValue: 'newKillmail',
    },
  });
});

ws.on('error', (error) => {
  console.error(`${moment().format()} :: WS Error Occured - ${error})`);
});

ws.on('close', () => {
  console.log(`${moment().format()} :: WS Closed`);
});
