const zkb = require('./functions/zkbHashController');

const run = async () => {
  try {
    let res = await zkb.getNextHashSetForQueue();
    console.log(res._id);
    do {
      res = await zkb.getNextHashSetForQueue();
      console.log(`Mapping ${res.zkbDateString} - ${res._id}`);
      let killmails = Object.keys(res.killmails);
      killmails = killmails.map((key) => {
        return {
          _id: key,
          killmail_id: key,
          killmail_hash: res.killmails[key],
          date: res.zkbDateString,
        };
      });
      await zkb.killmailQueueInsert(killmails);
      await zkb.setMigrated(res._id);
      console.log(`Completed: ${res._id}`);
      await sleep(100);
    } while (res !== null);
  } catch (err) {
    console.error(err);
  }
};

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

run();
